# Midas app: _XXX_

_one line app description including project it is based on_

_one line complementary information about app usage_

This repository is part of the project **Midas** (for Minimalist Docker/Alpine Server). More infos in the [project wiki](https://gitlab.ensimag.fr/groups/midas/-/wikis/home).